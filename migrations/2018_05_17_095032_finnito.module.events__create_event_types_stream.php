<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleEventsCreateEventTypesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'event_types',
         'title_column' => 'name',
         'translatable' => true,
         'trashable' => false,
         'searchable' => false,
         'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        "name" => [
            "required" => true,
        ],
        "slug" => [
            "required" => true,
            "unique" => true,
        ],
    ];

}
