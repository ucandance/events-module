<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleEventsCreateEventsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'Events',
         'title_column' => 'name',
         'translatable' => true,
         'trashable' => false,
         'searchable' => true,
         'sortable' => false,
         "versionable" => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [
            'translatable' => false,
            'required' => true,
        ],
        'slug' => [
            'unique' => true,
            'required' => true,
        ],
        "type",
        "logo",
        "banner",
        "start_time",
        "end_time",
        "student_price",
        "nonstudent_price",
        "door_price",
        "tickets",
        "facebook",
        "description",
        "location",
        "publish_date",
        "meta_title",
        "meta_description",
        "meta_image",
    ];

}
