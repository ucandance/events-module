<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleEventsAssignMetaFields extends Migration
{

    protected $stream = [
        'slug' => 'event_types',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        "meta_title",
        "meta_description",
        "meta_image",
    ];
}
