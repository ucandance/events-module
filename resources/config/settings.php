<?php

return [
    "events_meta_description" => [
        "type" => "anomaly.field_type.textarea",
        "config" => [
            "max" => "250",
        ],
    ],
];
