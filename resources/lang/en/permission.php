<?php

return [
    'event_types' => [
        'name'   => 'Event types',
        'option' => [
            'read'   => 'Can read event types?',
            'write'  => 'Can create/edit event types?',
            'delete' => 'Can delete event types?',
        ],
    ],
    'Events' => [
        'name'   => 'Events',
        'option' => [
            'read'   => 'Can read events?',
            'write'  => 'Can create/edit events?',
            'delete' => 'Can delete events?',
        ],
    ],
];
