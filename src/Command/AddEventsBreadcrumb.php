<?php namespace Finnito\EventsModule\Command;

use Anomaly\Streams\Platform\Routing\UrlGenerator;
use Anomaly\Streams\Platform\Ui\Breadcrumb\BreadcrumbCollection;

/**
 * Class AddEventsBreadcrumb
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class AddEventsBreadcrumb
{

    /**
     * Handle the command.
     *
     * @param BreadcrumbCollection $breadcrumbs
     * @param UrlGenerator $url
     */
    public function handle(BreadcrumbCollection $breadcrumbs, UrlGenerator $url)
    {
        $breadcrumbs->add(
            'finnito.module.events::breadcrumb.events',
            "/events"
        );
    }
}
