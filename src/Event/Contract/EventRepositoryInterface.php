<?php namespace Finnito\EventsModule\Event\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

/**
 * Class EventRepositoryInterface
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
interface EventRepositoryInterface extends EntryRepositoryInterface
{
    public function inYear($year);
    public function listOfYears();
    public function thisYearInType($type, $year);
    public function eventByTypeAndDate($type, $date);
    // public function live();
    // public function inYear($year);
    // public function upcomingCurrentYear($year);
    // public function pastCurrentYear($year);
    // public function getYears();
}
