<?php namespace Finnito\EventsModule\Event;

use Anomaly\Streams\Platform\Entry\EntryCollection;

/**
 * Class EventCollection
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventCollection extends EntryCollection
{

}
