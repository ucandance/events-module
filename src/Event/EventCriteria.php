<?php namespace Finnito\EventsModule\Event;

use Anomaly\Streams\Platform\Entry\EntryCriteria;

/**
 * Class EventCriteria
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventCriteria extends EntryCriteria
{

    /**
     * Return the next event.
     *
     * @return EventInterface
     */
    public function nextEvent()
    {
        return $this->query
            ->where("publish_date", "<=", date("Y-m-d H:i:s"))
            ->where("start_time", ">=", date("Y-m-d H:i:s"))
            ->orderBy("start_time", "ASC")
            ->first();
    }

    // public function live() {
    //  $this->query->where('publish_date', '<=', date('Y-m-d H:i:s'));

 //        return $this;
    // }

    // public function eventBySlug($slug)
    // {
    //     // SELECT
    //     //  *
    //     // FROM
    //     //  pyrocms.pyrotest_events_events
    //     // LEFT JOIN
    //     //  pyrocms.pyrotest_events_event_types
    //     // ON
    //     //  pyrocms.pyrotest_events_events.type_id
    //     //     =
    //     //     pyrocms.pyrotest_events_event_types.id
    //     // WHERE
    //     //  event_type_slug
    //     //     =
    //     //     "ball";

    //     $this->query
    //         ->select('events_events.*')
    //         ->join(
    //             "events_event_types",
    //             'events_events.type_id',
    //             '=',
    //             'events_event_types.id')
    //         ->where("event_type_slug", $slug);
    //     return $this;
    // }
}
