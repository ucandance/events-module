<?php namespace Finnito\EventsModule\Event;

use Finnito\EventsModule\Event\Contract\EventInterface;
use Finnito\EventsModule\EventType\EventTypeModel;
use Anomaly\Streams\Platform\Model\Events\EventsEventsEntryModel;

/**
 * Class EventModel
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventModel extends EventsEventsEntryModel implements EventInterface
{
    /**
     * Eager load these relations.
     *
     * @var array
     */
    protected $with = [
        "type",
    ];

    /**
     * Get event prices as
     * a <br> delimited
     * string of HTML.
     *
     * @return null|string
     */
    public function getPrices()
    {
        $priceArray = array();
        if (!is_null($this->student_price)) {
            array_push($priceArray, "Student: \${$this->student_price}");
        }
        if (!is_null($this->nonstudent_price)) {
            array_push($priceArray, "Non-Student: \${$this->nonstudent_price}");
        }
        if (!is_null($this->door_price)) {
            array_push($priceArray, "Door: \${$this->door_price}");
        }
        return implode("<br>", $priceArray);
    }

    /**
     * Return if the event is live or not.
     *
     * @return bool
     */
    public function isLive()
    {
        return ($this->publish_date <= date("Y-m-d H:i:s"));
    }

    /**
     * Return the category of the event
     *
     * @return null|EventTypeInterface
     */
    public function category()
    {
        return $this->belongsTo(EventTypeModel::class, "type_id")->first();
    }
}
