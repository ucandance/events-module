<?php namespace Finnito\EventsModule\Event;

use Anomaly\Streams\Platform\Entry\EntryObserver;

/**
 * Class EventObserver
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventObserver extends EntryObserver
{

}
