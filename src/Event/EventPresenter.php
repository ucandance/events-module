<?php namespace Finnito\EventsModule\Event;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

/**
 * Class EventPresenter
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventPresenter extends EntryPresenter
{

    /**
     * Return the status of the event
     * as a string of HTML.
     *
     * @return null|string
     */
    public function statusLabel()
    {
        $size = 'sm';
        $color  = 'default';
        $status = $this->status();
        $pub = "";

        switch ($status) {
            case 'Scheduled':
                $color = 'info';
                $pub = " for " . $this->object->publish_date->format('M jS, g:ia');
                break;

            case 'Draft':
                $color = 'default';
                break;

            case 'Live':
                $color = 'success';
                break;
        }

        return '<span class="tag tag-' . $size . ' tag-' . $color . '">' .  $status . $pub . '</span>';
    }

    /**
     * Return the status of the event
     *
     * @return bool
     */
    public function status()
    {
        if ($this->object->isLive()) {
            return 'Live';
        } else {
            return 'Scheduled';
        }

        return null;
    }

    /**
     * Return category of the event
     *
     * @return null|EventTypeInterface
     */
    public function category()
    {
        return $this->object->category();
    }
}
