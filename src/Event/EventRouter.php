<?php namespace Finnito\EventsModule\Event;

use Anomaly\Streams\Platform\Entry\EntryRouter;

/**
 * Class EventRouter
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventRouter extends EntryRouter
{

}
