<?php namespace Finnito\EventsModule\Event;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;

/**
 * Class EventSeeder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventSeeder extends Seeder
{

    /**
     * Run the seeder.
     */
    public function run()
    {
        //
    }
}
