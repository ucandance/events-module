<?php namespace Finnito\EventsModule\Event\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

/**
 * Class EventTableBuilder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [
        "search" => [
            "fields" => [
                "name",
                "type",
                "description",
            ],
        ],
        "type",
    ];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        "name",
        "type",
        "price" => [
            "heading" => "Price",
            "value" => "entry.getPrices()|raw",
        ],
        "start_time" => [
            "value" => "entry.start_time"
        ],
        "live" => [
            "heading" => "Status",
            "value" => "entry.statusLabel()",
        ],
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit'
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [
        "order_by" => [
            "publish_date" => "DESC",
        ],
    ];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];
}
