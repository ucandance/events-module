<?php namespace Finnito\EventsModule\EventType\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

/**
 * Class EventTypeRepositoryInterface
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
interface EventTypeRepositoryInterface extends EntryRepositoryInterface
{

}
