<?php namespace Finnito\EventsModule\EventType;

use Anomaly\Streams\Platform\Entry\EntryCollection;

/**
 * Class EventTypeCollection
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventTypeCollection extends EntryCollection
{

}
