<?php namespace Finnito\EventsModule\EventType;

use Anomaly\Streams\Platform\Entry\EntryCriteria;

/**
 * Class EventTypeCriteria
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventTypeCriteria extends EntryCriteria
{

}
