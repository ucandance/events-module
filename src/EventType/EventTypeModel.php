<?php namespace Finnito\EventsModule\EventType;

use Finnito\EventsModule\EventType\Contract\EventTypeInterface;
use Anomaly\Streams\Platform\Model\Events\EventsEventTypesEntryModel;
use Finnito\EventsModule\Event\EventModel;

/**
 * Class EventTypeModel
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventTypeModel extends EventsEventTypesEntryModel implements EventTypeInterface
{

    public function events()
    {
        return $this
            ->hasMany(EventModel::class, "type_id")
            ->orderBy("start_time", "DESC")
            ->get();
    }
}
