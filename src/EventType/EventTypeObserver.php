<?php namespace Finnito\EventsModule\EventType;

use Anomaly\Streams\Platform\Entry\EntryObserver;

/**
 * Class EventTypeObserver
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventTypeObserver extends EntryObserver
{

}
