<?php namespace Finnito\EventsModule\EventType;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

/**
 * Class EventTypePresenter
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventTypePresenter extends EntryPresenter
{

}
