<?php namespace Finnito\EventsModule\EventType;

use Finnito\EventsModule\EventType\Contract\EventTypeRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

/**
 * Class EventTypeRepository
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventTypeRepository extends EntryRepository implements EventTypeRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var EventTypeModel
     */
    protected $model;

    /**
     * Create a new EventTypeRepository instance.
     *
     * @param EventTypeModel $model
     */
    public function __construct(EventTypeModel $model)
    {
        $this->model = $model;
    }
}
