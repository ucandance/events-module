<?php namespace Finnito\EventsModule\EventType;

use Anomaly\Streams\Platform\Entry\EntryRouter;

/**
 * Class EventTypeRouter
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventTypeRouter extends EntryRouter
{

}
