<?php namespace Finnito\EventsModule\EventType;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;

/**
 * Class EventTypeSeeder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventTypeSeeder extends Seeder
{

    /**
     * Run the seeder.
     */
    public function run()
    {
        //
    }
}
