<?php namespace Finnito\EventsModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

/**
 * Class EventsModule
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventsModule extends Module
{

    /**
     * The navigation display flag.
     *
     * @var bool
     */
    protected $navigation = true;

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'glyphicons glyphicons-global';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'events' => [
            'buttons' => [
                'new_event',
            ],
        ],
        'event_types' => [
            'buttons' => [
                'new_event_type',
            ],
        ],
        "configuration",
    ];
}
