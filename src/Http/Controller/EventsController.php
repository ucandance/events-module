<?php namespace Finnito\EventsModule\Http\Controller;

use Finnito\EventsModule\EventType\Contract\EventTypeRepositoryInterface;
use Anomaly\SettingsModule\Setting\Contract\SettingRepositoryInterface;
use Finnito\EventsModule\Event\Contract\EventRepositoryInterface;
use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Finnito\EventsModule\Event\Command\AddTemplateEditLink;
use Finnito\EventsModule\Event\Table\EventTableBuilder;
use Finnito\EventsModule\Event\Form\EventFormBuilder;
use Finnito\EventsModule\Command\AddEventsBreadcrumb;

/**
 * Class EventsController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class EventsController extends PublicController
{
    /**
     * Return an index of all current year events.
     *
     * @return \Illuminate\Contracts\View\View|mixed
     */
    public function index(
        SettingRepositoryInterface $settings,
        EventRepositoryInterface $events
    ) {
        $year = date("Y");
        $yearEvents = $events->inYear($year);
        $this->dispatch(new AddEventsBreadcrumb());
        $this->template->set('meta_title', 'finnito.module.events::breadcrumb.events');
        $this->template->set("meta_description", $settings->value('finnito.module.events::events_meta_description'));

        if ($yearEvents->isNotEmpty()) {
             $this->template->set("meta_image", $yearEvents[0]->meta_image->id);
        }

        return $this->view->make(
            "finnito.module.events::public/index",
            [
                "year" => $year,
                "events" => $yearEvents,
                "years" => $events->listOfYears(),
            ]
        );
    }

    /**
     * Return an index of events of a given type.
     *
     * @param Finnito\EventsModule\EventType\Contract\EventTypeRepositoryInterface
     * @param Finnito\EventsModule\Event\Contract\EventRepositoryInterface
     * @param $type
     * @return \Illuminate\Contracts\View\View|mixed
     */
    public function type(
        EventTypeRepositoryInterface $eventTypes,
        EventRepositoryInterface $events,
        $type
    ) {
        if (!$eventType = $eventTypes->findBy("slug", $type)) {
            abort(404);
        }

        $year = date("Y");

        $this->dispatch(new AddEventsBreadcrumb());
        $this->breadcrumbs->add(
            $eventType->name,
            $this->request->path()
        );
        $this->template->set('meta_title', "Events › {$eventType->meta_title}");
        $this->template->set("meta_description", $eventType->meta_description);
        $this->template->set("meta_image", $eventType->meta_image);

        $event = $events->thisYearInType($type, $year);
        if (!is_null($event)) {
            $this->template->set('edit_link', "/admin/events/edit/".$event->id);
            $this->template->set("edit_type", $event->name);
        }

        return $this->view->make(
            "finnito.module.events::public/type",
            [
                "currentYear" => $year,
                "type" => $eventType,
                "currentYearEvents" => $events->inYear($year),
                "event" => $event,
            ]
        );
    }

    /**
     * Return an index of events for a given year.
     *
     * @param Finnito\EventsModule\Event\Contract\EventRepositoryInterface
     * @param $year
     * @return \Illuminate\Contracts\View\View|mixed
     */
    public function year(
        EventRepositoryInterface $events,
        $year
    ) {

        $years = $events->listOfYears();
        if (!$years->contains($year)) {
            abort(404);
        }

        $currentYearEvents = $events->inYear(date("Y"));

        $this->dispatch(new AddEventsBreadcrumb());
        $this->breadcrumbs->add(
            $year,
            "/events/{$year}"
        );
        $this->template->set('meta_title', "Events › {$year}");
        $this->template->set("meta_description", "UCanDance events for the year of {$year}!");

        if ($currentYearEvents->isNotEmpty()) {
            $this->template->set("meta_image", $currentYearEvents[0]->meta_image->id);
        }

        return $this->view->make(
            "finnito.module.events::public/year",
            [
                "year" => $year,
                "currentYear" => date("Y"),
                "events" => $events->inYear($year),
                "currentYearEvents" => $currentYearEvents,
                "years" => $years,
            ]
        );
    }

    /**
     * Return a single event.
     *
     * @param Finnito\EventsModule\Event\Contract\EventRepositoryInterface
     * @param $type
     * @param $date
     * @return \Illuminate\Contracts\View\View|mixed
     */
    public function single(
        EventRepositoryInterface $events,
        $type,
        $date
    ) {
        if (!$event = $events->eventByTypeAndDate($type, $date)) {
            abort(404);
        }

        $year = date("Y");

        $this->dispatch(new AddEventsBreadcrumb());
        $this->breadcrumbs->add(
            $event->type->name,
            "/events/{$event->type->slug}"
        );
        $this->breadcrumbs->add(
            $event->start_time->format('Y'),
            "/events/year/{$event->start_time->format('Y')}"
        );
        $this->breadcrumbs->add(
            $event->name,
            $this->request->path()
        );
        $this->template->set('meta_title', "{$event->meta_title}");
        $this->template->set("meta_description", $event->meta_description);
        $this->template->set("meta_image", $event->meta_image->id);

        $this->template->set('edit_link', "/admin/events/edit/".$event->id);
        $this->template->set("edit_type", $event->name);
        // Render!
        return $this->view->make(
            "finnito.module.events::public/single",
            [
                "currentYear" => $year,
                "event" => $event,
                "currentYearEvents" => $events->inYear($year),
                "date" => $date,
            ]
        );
    }
}
